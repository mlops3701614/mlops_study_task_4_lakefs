DATASETS = ['housing_1', 'housing_2']

rule all:
    input:
        expand("data/lakefs/raw_{dataset}.csv", dataset=DATASETS),
        expand("data/{dataset}.txt", dataset=DATASETS),
        expand(["data/processed_onehot/{dataset}_train.csv",
                "data/processed_onehot/{dataset}_test.csv"], dataset=DATASETS)


rule upload_data:
    output:
        "data/{dataset}.txt"
    shell:
        """
        docker exec lakefs-example-lakefs-1 lakectl fs upload -s /data/raw_{wildcards.dataset}.csv lakefs://myrepo/main/data/raw_{wildcards.dataset}.csv
        docker exec lakefs-example-lakefs-1 lakectl commit lakefs://myrepo/main -m 'Commit message'
        echo $null > data/{wildcards.dataset}.txt
        """


rule read_data_from_lakefs:
    output:
        "data/lakefs/raw_{dataset}.csv"
    shell:
        "docker exec lakefs-example-lakefs-1 lakectl fs cat lakefs://myrepo/main/data/raw_{wildcards.dataset}.csv > {output}"

rule preprocessing_data_onehot:
    input:
        "data/lakefs/raw_{dataset}.csv"
    output:
        train = "data/processed_onehot/{dataset}_train.csv",
        test = "data/processed_onehot/{dataset}_test.csv"
    threads: 2
    script:
        "scripts/preprocess_data_onehot.py"
